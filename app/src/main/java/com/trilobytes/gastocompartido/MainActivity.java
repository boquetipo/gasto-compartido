package com.trilobytes.gastocompartido;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity implements NumberPicker.OnValueChangeListener {

    Menu mainMenu;
    String[] listDistance;
    String[] listCurrency;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // get favourite units or default
        Preferences.init(this);

        // init units and labels
        initSettings();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                calculate();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        mainMenu = menu;

        MenuItem distanceMenuItem;

        distanceMenuItem = menu.findItem(R.id.action_distance);
        distanceMenuItem.setTitle(MainActivity.this.getString(R.string.labelDistance, Preferences.getDistance()));

        distanceMenuItem = menu.findItem(R.id.action_currency);
        distanceMenuItem.setTitle(MainActivity.this.getString(R.string.labelCurrency, Preferences.getCurrency()));

        EditText passengers = findViewById(R.id.valPassengers);

        passengers.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    showDialogPassengers(v);
                }
            }
        });

        passengers.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                showDialogPassengers(v);
            }
        });

        return true;
    }

    @Override
    public void onValueChange(NumberPicker numberPicker, int i, int i1) {
        // number picker on change listener
        EditText passengers = findViewById(R.id.valPassengers);
        passengers.setText(numberPicker.getValue() + "");

        LinearLayout dummy = findViewById(R.id.dummy);
        dummy.requestFocus();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // drop-down menu (settings)

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        AlertDialog.Builder mBuilder;
        AlertDialog mDialog;

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_distance:
                //TODO SELECT CURRENT VALUE
                int selected = 0;

                mBuilder = new AlertDialog.Builder(MainActivity.this);
                mBuilder.setTitle(MainActivity.this.getString(R.string.chooseUnitDistance));
                mBuilder.setSingleChoiceItems(listDistance, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Preferences.setDistance(listDistance[i]);

                        // distance label
                        TextInputLayout til = findViewById(R.id.labelDistance);
                        til.setHint(MainActivity.this.getString(R.string.labelDistance, listDistance[i]));

                        // consumption label
                        til = findViewById(R.id.labelConsumption);
                        til.setHint(MainActivity.this.getString(R.string.labelConsumption, listDistance[i]));

                        // drop-down menu item
                        MenuItem distanceMenuItem;
                        distanceMenuItem = mainMenu.findItem(R.id.action_distance);
                        distanceMenuItem.setTitle(MainActivity.this.getString(R.string.labelDistance, Preferences.getDistance()));

                        dialogInterface.dismiss();
                    }
                });

                mDialog = mBuilder.create();
                mDialog.show();
            break;
            case R.id.action_currency:
                mBuilder = new AlertDialog.Builder(MainActivity.this);
                mBuilder.setTitle(MainActivity.this.getString(R.string.chooseUnitCurrency));
                mBuilder.setSingleChoiceItems(listCurrency, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Preferences.setCurrency(listCurrency[i]);

                        MenuItem distanceMenuItem;
                        distanceMenuItem = mainMenu.findItem(R.id.action_currency);
                        distanceMenuItem.setTitle(MainActivity.this.getString(R.string.labelCurrency, Preferences.getCurrency()));

                        TextInputLayout til = findViewById(R.id.labelPrice);
                        til = findViewById(R.id.labelPrice);
                        til.setHint(MainActivity.this.getString(R.string.labelPrice, Preferences.getCurrency()));

                        // recalculate
                        calculate();

                        dialogInterface.dismiss();
                    }
                });

                mDialog = mBuilder.create();
                mDialog.show();
            break;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Init all selecting items
     * and parse labels with units
     */
    private void initSettings() {
        // distance
        listDistance = getResources().getStringArray(R.array.distance_item);

        // currency
        listCurrency = getResources().getStringArray(R.array.currency_item);

        TextInputLayout til = findViewById(R.id.labelDistance);
        til.setHint(MainActivity.this.getString(R.string.labelDistance, Preferences.getDistance()));

        til = findViewById(R.id.labelConsumption);
        til.setHint(MainActivity.this.getString(R.string.labelConsumption, Preferences.getDistance()));

        til = findViewById(R.id.labelPrice);
        til.setHint(MainActivity.this.getString(R.string.labelPrice, Preferences.getCurrency()));
    }

    /**
     * Shows number picker dialog
     * @param v View
     */
    public void showDialogPassengers(View v) {
        NumberPickerDialog newFragment = new NumberPickerDialog();
        newFragment.setValueChangeListener(MainActivity.this);
        newFragment.show(getSupportFragmentManager(), "number picker");
    }

    /**
     * Calculates money to pay per passenger
     */
    private void calculate() {
        EditText editDistance = findViewById(R.id.valDistance);
        EditText editPassengers = findViewById(R.id.valPassengers);
        EditText editConsumption = findViewById(R.id.valConsumption);
        EditText editPrice = findViewById(R.id.valPrice);

        String distance = editDistance.getText().toString();
        String passengers = editPassengers.getText().toString();
        String consumption = editConsumption.getText().toString();
        String price = editPrice.getText().toString();

        boolean isOk = checkValues(distance, passengers, consumption, price);

        TextView result = findViewById(R.id.result);

        if (!isOk) {
            result.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
            result.setText(MainActivity.this.getString(R.string.errorInput));
        } else {
            String total = calculate(distance, passengers, consumption, price);

            result.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 24);
            result.setText(total + Preferences.getCurrency());
        }
    }

    /**
     * Checks if form values are correct
     * @param distance
     * @param passengers
     * @param consumption
     * @param price
     * @return
     */
    private boolean checkValues(String distance, String passengers, String consumption, String price) {
        if (!isNumeric(distance)) {
            // error
            return false;
        }
        if (!isNumeric(passengers)) {
            // error
            return false;
        }
        if (!isNumeric(consumption)) {
            // error
            return false;
        }
        if (!isNumeric(price)) {
            // error
            return false;
        }

        return true;
    }

    /**
     * Checks if given value is numeric
     * @param val
     * @return
     */
    private boolean isNumeric(String val) {
        return val.matches("\\d+(.\\d+)?");
    }

    /**
     * Calculates money to pay per passenger
     * @param distance
     * @param passengers
     * @param consumption
     * @param price
     * @return
     */
    private String calculate(String distance, String passengers, String consumption, String price) {
        Double temp = Double.parseDouble(consumption) * Double.parseDouble(distance) / 100;
        Double total = (Double.parseDouble(price) * temp) / Integer.parseInt(passengers);
        DecimalFormat df = new DecimalFormat("#.##");

        return df.format(total);
    }
}
