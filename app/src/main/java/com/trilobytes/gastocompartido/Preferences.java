package com.trilobytes.gastocompartido;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Class to store/retreive favourite units to/from shared preferences
 */
public class Preferences {

    private static MainActivity activity;

    /**
     * Gets favourite units or defaults
     * @param anActivity
     */
    public static void init(MainActivity anActivity) {
        Preferences.activity = anActivity;

        Preferences.getDistance();
        Preferences.getCurrency();
    }
    
    /**
     * Get distance preference
     * @return String
     */
    public static String getDistance(){
        SharedPreferences sharedPref = activity.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE);
        return sharedPref.getString("distance", "mi");
    }

    /**
     * Get currency preference
     * @return String
     */
    public static String getCurrency(){
        SharedPreferences sharedPref = activity.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE);
        return sharedPref.getString("currency", "$");
    }

    /**
     * Set distance preference
     * @param distance String
     */
    public static void setDistance(String distance){
        SharedPreferences sharedPref = activity.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        editor.putString("distance", distance);
        editor.commit();
    }

    /**
     * Set distance preference
     * @param currency String
     */
    public static void setCurrency(String currency){
        SharedPreferences sharedPref = activity.getSharedPreferences(BuildConfig.APPLICATION_ID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        editor.putString("currency", currency);
        editor.commit();
    }
}
